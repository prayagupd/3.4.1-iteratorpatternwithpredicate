package functor;

import java.util.Enumeration;
import java.util.List;

/**
 * Created by 984493
 * on 4/4/2015.
 */

public class SumFunctor implements Functor<Integer, Integer> {
    private Integer accumulate = 0;

    public SumFunctor(){}

    public void apply(Integer element) {
                //System.out.println("|________accumulating " + element + " to " + accumulate + "  => " + (accumulate+element));
                accumulate += element;
    }

    public Integer getValue() {
        return accumulate;
    }
}
