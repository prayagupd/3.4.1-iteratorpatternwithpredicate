package functor;

/**
 * Created by 984493
 * on 4/4/2015.
 */

public class AverageFunctor implements Functor<Integer, Double> {
    private int size = 0;
    private int accumulate = 0;

    public AverageFunctor(){}

    public void apply(Integer element) {
                accumulate += element;
                size++;
    }

    public Double getValue() {
        return (accumulate*1.0/size*1.0);
    }
}
