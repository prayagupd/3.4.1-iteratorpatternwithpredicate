package functor;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Vector;
import predicate.Predicate;

/**
 * Created by 984493
 * on 4/4/2015.
 */

public class MyVector<T> extends Vector<T> {

    //internal iterator
    public Double doAll(Functor functor) {
        //get it from iterator

        for(T element : this) {
            functor.apply(element);
        }

        return Double.valueOf(functor.getValue() + "");
    }

    public Double doAll(Predicate predicate, Functor functor) {
        MyVector<T> vector = new MyVector<T>();
        Iterator<T> e = iterator();
        while(e.hasNext()) {
          T next = e.next();
          if (predicate.check(next)) {
            functor.apply(next);
          }
        }
       return (Double.valueOf(functor.getValue()+""));
    }

    public Iterator<T> getIterator(Predicate predicate){
      Vector<T> newVector = new MyVector<T>();
      for(T element : this) {
        if (predicate.check(element)) {
          //System.out.println("|____________ " + element);
          newVector.add(element);
        }
      }
      MyIterator<T> iterator = new MyIterator(newVector);
      return iterator;
    }

    public class MyIterator<T> implements Iterator {
      private Vector<T> newVector = new MyVector<T>();
      private int index = 0;
      MyIterator(Vector<T> iterationVector){
        this.newVector = iterationVector;
      }

      @Override
      public boolean hasNext() {
        if (index < newVector.size()) {
          return true;
        }
        return false;
      }

      @Override
      public T next() {
        if (hasNext()) {
          return (newVector.get(index++));
        }
        return null;
      }
    }
}
