package functor;

import java.util.Iterator;
import predicate.GreatherThanPredicate;

/**
 * Created by 984493
 * on 4/4/2015.
 */

public class IterationApp {
    public static void main(String[] args){
        MyVector<Integer> vector = new MyVector<Integer>();
        vector.add(1);
        vector.add(2);
        vector.add(3);
        vector.add(4);
        vector.add(5);
        vector.add(6);
        vector.add(7);
        vector.add(8);
        vector.add(9);
        vector.add(10);

        System.out.println("===================== 1 =======================");
        double sum = vector.doAll(new SumFunctor());
        System.out.println("Sum : " + sum);

        double average = vector.doAll(new AverageFunctor());
        System.out.println("Average : " + average);

        System.out.println("===================== 2 =======================");
        double sumPredicate = vector.doAll(new GreatherThanPredicate().criteria(2),new SumFunctor());
        System.out.println("Sum Predicate : " + sumPredicate);

        double averagePredicate = vector.doAll(new GreatherThanPredicate().criteria(2),new AverageFunctor());
        System.out.println("Average Predicate : " + averagePredicate);

        System.out.println("===================== 3 =======================");
        Iterator<Integer> iterator = vector.getIterator(new GreatherThanPredicate().criteria(2));
        Integer sumIterable = 0;
        Integer size = 0;
        while (iterator.hasNext()) {
          Integer i = iterator.next();
          sumIterable+=i;
          size++;
        }
        Double averageIterator = sumPredicate / size;
        System.out.println("Sum with Predicate and Iterator: " + sumIterable);
        System.out.println("Average with Predicate and Iterator: " + averageIterator);

    }
}
