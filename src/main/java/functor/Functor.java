package functor;

/**
 * Created by 984493
 * on 4/4/2015.
 */

public interface Functor<T, R> {
    public void apply(T element);
    public R getValue();
}
