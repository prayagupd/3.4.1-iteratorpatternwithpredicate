package predicate;

/**
 * Created by prayagupd
 * on 4/4/15.
 */

public interface Predicate<T> {
  public boolean check(T element);
}
