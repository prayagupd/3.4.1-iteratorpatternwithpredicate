package predicate;

/**
 * Created by prayagupd
 * on 4/5/15.
 */

public class GreatherThanPredicate implements Predicate<Integer> {
  private Integer criteria;

  @Override
  public boolean check(Integer element) {
    //System.out.println("element " + element +" > " + criteria + " => " + (element>criteria));
    if (element > criteria) {
      return true;
    }
    return false;
  }

  public GreatherThanPredicate criteria(Integer greatherThan) {
    this.criteria = greatherThan;
    return this;
  }

}
